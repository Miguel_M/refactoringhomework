function createStatementData(invoice,plays) {
    const result = {};
    result.customer=invoice.customer;
    result.performances= invoice.performances.map(enrichPerformance);
    result.totalAmount = totalAmount(result,plays);
    result.totalVolumeCredits =totalVolumeCredits(result,plays);
    return result;
}
function enrichPerformance(aPerformance,plays) {
    const result = Object.assign({},aPerformance);
    result.play= playFor(result,plays);
    //   result.amount = amountFor(result); por alguna razon no funciona...
    //  result.volumeCredits = volumeCreditsFor(result,plays); lo mismo
    return result;

}


function playFor(aPerformance,plays) {

    return plays[aPerformance.playID];
}

function amountFor(aPerfomance, plays) {
    let result = 0;
    switch (plays[aPerfomance.playID].type) {
        case "tragedy":
            result  = 40000;
            if (aPerfomance.audience > 30) {
                result += 1000 * (aPerfomance.audience - 30);
            }
            break;
        case "comedy":
            result = 30000;
            if (aPerfomance.audience > 20) {
                result += 10000 + 500 * (aPerfomance.audience - 20);
            }
            result += 300 * aPerfomance.audience;
            break;
        default:
            throw new Error(`unknown type: ${plays[aPerfomance.playID].type}`);
    }
    return result;
}

function totalVolumeCredits(data,plays) {
    let result=0;
    for (let perf of data.performances) {
        result += volumeCreditsFor(perf,plays);
    }
    return result;
    //Aqui deberia usar return data.performances.reduce((total, p) => total + p.volumeCredits, 0);
}

function totalAmount(data, plays) {
    let result = 0;
    for (let perf of data.performances) {
        result += amountFor(perf,plays);
    }
    return result;
    //Aqui deberia usar   return data.performances.reduce((total, p) => total + p.amount, 0); pero no funciona
}

function volumeCreditsFor(aPerformance,plays) {
    let result=0;
    result += Math.max(aPerformance.audience - 30, 0);
    if ("comedy" === playFor(aPerformance,plays).type) result +=
        Math.floor(aPerformance.audience / 5);
    return result;
}

export default createStatementData;